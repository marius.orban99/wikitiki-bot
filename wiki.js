const fs = require('fs');
const wikijs = require('wikijs').default;
const jsdom = require('jsdom');
const { JSDOM } = jsdom;

class Wiki {
	constructor(wikiApiUrl) {
		this.aliases = {};
		this._aliasesInput = {};
		this.aliasesLowerCase = {};
		this.caseSensitiveAliases = false;
		this.wiki = wikijs({
			apiUrl: wikiApiUrl,
			origin: null,
		});
	}

	loadAliasesFromJson(filepath) {
		try {
			const content = fs.readFileSync(filepath);
			this._aliasesInput = JSON.parse(content);
			const aliasesInput = this._aliasesInput.aliases;
			for (const wikiPage in aliasesInput) {
				for (const alias of aliasesInput[wikiPage]) {
					if (alias in this.aliases) {
						console.error(`Alias ${alias} already mapped to wiki page ${wikiPage}, only first entry will be kept!`);
					}
					else {
						this.aliases[alias] = wikiPage;
						const lowerCaseAlias = alias.toLowerCase();
						if (alias !== lowerCaseAlias) {
							this.aliasesLowerCase[lowerCaseAlias] = wikiPage;
						}
					}
				}
			}
		}
		catch (err) {
			console.error(err.toString());
		}
	}

	setCaseSensitiveAliases(isCaseSensitive) {
		this.caseSensitiveAliases = isCaseSensitive;
	}

	getPageForAliasOrNull(alias) {
		if (!alias) {
			console.error('getPageForAliasOrNone: empty alias was given');
			return null;
		}

		if (alias in this.aliases) {
			return this.aliases[alias];
		}

		if (!this.caseSensitiveAliases) {
			if(alias.toLowerCase() in this.aliasesLowerCase) {
				return this.aliasesLowerCase[alias.toLowerCase()];
			}
		}

		return null;
	}
}

module.exports = Wiki;