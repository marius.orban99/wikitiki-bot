const Wiki = require('../wiki.js');
const { MessageEmbed } = require('discord.js');
const { SlashCommandBuilder } = require('@discordjs/builders');

// Read local test data
const cryptTvWiki = new Wiki('https://crypttv.fandom.com/api.php');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('help')
		.setDescription('Brief guide on how to use the CryptTv wiki bot.'),
	async execute(interaction) {
		const helpEmbed = new MessageEmbed();
		helpEmbed.setTitle('How to use the live wiki command');
		helpEmbed.setColor('#ED1A3A');
		helpEmbed.setImage('https://cdn.discordapp.com/attachments/435180634638647316/878721135225757716/crypttv_wiki_bot_guide.png');
		helpEmbed.setFooter('Crypt TV Official', 'https://upload.wikimedia.org/wikipedia/commons/b/bc/Crypt_TV_logo_%282018%29.jpg');
		await interaction.reply({ embeds: [helpEmbed] });
	},
};