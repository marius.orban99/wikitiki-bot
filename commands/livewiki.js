const Wiki = require('../wiki.js');
const { MessageEmbed } = require('discord.js');
const { SlashCommandBuilder } = require('@discordjs/builders');
const { JSDOM } = require('jsdom');


// Read local test data
const cryptTvWiki = new Wiki('https://crypttv.fandom.com/api.php');
cryptTvWiki.loadAliasesFromJson('./assets/livewiki_aliases.json');
cryptTvWiki.setCaseSensitiveAliases(false);


module.exports = {
	data: new SlashCommandBuilder()
		.setName('livewiki')
		.setDescription('Fetches information about the CryptTv universe directly from the wiki!')
		.addStringOption(option => {
			option.setName('page_title')
				.setDescription('(Case Sensitive) The exact title of the CryptTv wiki page you wish to see')
				.setRequired(true);

			for (const page in cryptTvWiki._aliasesInput.aliases) {
				option.addChoice(page.replaceAll('_', ' '), page);
			}

			return option;
		},
		),
	async execute(interaction) {
		const pageTitleOption = interaction.options.getString('page_title');
		const wikiPage = cryptTvWiki.getPageForAliasOrNull(pageTitleOption) ?? interaction.options.getString('page_title').replaceAll(' ', '_');
		cryptTvWiki.wiki.api({
			action: 'parse',
			page: wikiPage,
		}).then(async res => {
			const pageData = res.parse;
			const dom = new JSDOM(pageData.text['*']);
			const resultTitle = dom.window.document.querySelector('h2').textContent;
			const resultImage = dom.window.document.querySelector('img.pi-image-thumbnail').attributes.src.value;
			// Parse infobox
			const infoStuff = new Map();
			const resultPiItems = dom.window.document.querySelectorAll('div.pi-item');
			resultPiItems.forEach(item => {
				const infoKey = item.children[0].textContent;
				let infoValue = '';

				function _recurseInfoValues(element) {
					if (element.children) {
						if (element.children.length == 0) {
							if (element.textContent) {
								infoValue += element.textContent + '; ';
							}
							return;
						}
						else {
							for (const child of element.children) {
								_recurseInfoValues(child);
							}
						}
					}
				}

				const valueElement = item.children[1];
				_recurseInfoValues(valueElement);
				infoStuff.set(infoKey, infoValue.substr(0, infoValue.lastIndexOf(';')));
			});

			const resultEmbed = new MessageEmbed();
			resultEmbed.setTitle(resultTitle);
			resultEmbed.setColor('#ED1A3A');
			resultEmbed.setURL(`https://crypttv.fandom.com/wiki/${wikiPage}`);
			resultEmbed.setFooter(`Crypt TV Official | Read more: https://crypttv.fandom.com/wiki/${wikiPage}`,
				'https://upload.wikimedia.org/wikipedia/commons/b/bc/Crypt_TV_logo_%282018%29.jpg');
			// resultEmbed.setURL(testEntry._source);
			infoStuff.forEach((value, key) => {
				resultEmbed.addField(key, value);
			});
			resultEmbed.setImage(resultImage);
			// resultEmbed.setFooter(`Source: ${testEntry._source} | Result for query ${args}`);
			// message.channel.send(resultEmbed);
			await interaction.reply({ embeds: [resultEmbed] });
		});
		// await interaction.reply({ embeds: [resultEmbed] });
	},
};