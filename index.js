const fs = require('fs');
const dotenv = require('dotenv');
const { Client, Collection, Intents, MessageEmbed } = require('discord.js');
const { REST } = require('@discordjs/rest');
const { Routes } = require('discord-api-types/v9');
const jsdom = require('jsdom');
const { JSDOM } = jsdom;
const Wiki = require('./wiki.js');
const { info } = require('console');

dotenv.config();

const WIKIBOT_TEXT_COMMAND_PREFIX = '.';
// SLASH COMMANDS WILL ONLY BE SUPPORTED STARTING WITH discord.js v13.0.0
const WIKIBOT_ENABLE_SLASH_COMMANDS = true;

const client = new Client({ intents: [Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MESSAGES] });
client.commands = new Collection();


// Set up commands defined in the ./commands folder and optionally register as slash-commands
// SLASH COMMANDS WILL ONLY BE SUPPORTED STARTING WITH discord.js v13.0.0
const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));

for (const file of commandFiles) {
	const command = require(`./commands/${file}`);
	// set a new item in the Collection
	// with the key as the command name and the value as the exported module
	client.commands.set(command.data.name, command);
}

// Read local test data
const cryptTvWiki = new Wiki('https://crypttv.fandom.com/api.php');
cryptTvWiki.loadAliasesFromJson('./assets/livewiki_aliases.json');

client.once('ready', async () => {
	console.log('Ready!');
});

client.on('messageCreate', message => {
	// If in debug mode ignore messages outside the debug server
	// Or if in production ignore messages from the debug server
	if (process.env.WIKITIKI_DEBUG_MODE === 'true') {
		if (message.guild.id != process.env.WIKITIKI_DEBUG_SERVER) {
			return;
		}
	}
	else if (process.env.WIKITIKI_DEBUG_SERVER && process.env.WIKITIKI_DEBUG_SERVER == message.guild.id) {
		return;
	}

	// Don't handle empty messages
	if (message.content.length === 0) {
		return;
	}

	// Trim whitespaces and handle message as command if prefix is present
	message.content = message.content.trim();
	const args = message.content.split(' ');
	if (args[0][0] === WIKIBOT_TEXT_COMMAND_PREFIX) {
		const command = args[0].substr(1);
		console.log(`Legacy command ${WIKIBOT_TEXT_COMMAND_PREFIX}${command} triggered by ${message.author.username} | args=${args} | ` +
					`guild=${message.guild.name} (${message.guild.id}) | channel=${message.channel.name} (${message.channel.id})`);

		try {
			// Legacy commands
			// if (command === 'ping') {
			// 	message.channel.send('Pong!');
			// }

			if (command === 'registerGlobalSlashCommands') {
				const commands = [];
				const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));

				for (const file of commandFiles) {
					const command = require(`./commands/${file}`);
					commands.push(command.data.toJSON());
				}

				const rest = new REST({ version: '9' }).setToken(process.env.WIKITIKI_BOT_TOKEN);

				(async () => {
					try {
						console.log('Started refreshing application (/) commands.');

						await rest.put(
							Routes.applicationCommands(process.env.WIKITIKI_APPLICATION_ID),
							{ body: commands },
						);

						console.log('Successfully reloaded application (/) commands.');
					} catch (error) {
						console.error(error);
					}
				})();
			}
		} catch (err) {
			console.error(`Exception occured while running the command:\n${err.toString()}`);
		}
	}
});


// This will work starting discord.js v13.0.0
client.on('interactionCreate', async interaction => {
	if (!interaction.isCommand()) return;
	const { commandName } = interaction;
	if (!client.commands.has(commandName)) return;

	console.log(`Slash command ${commandName} triggered by ${interaction.user.username} | ` +
				`guild=${interaction.guild.name} (${interaction.guild.id}) | channel=${interaction.channel.name} (${interaction.channel.id})`);
	try {
		await client.commands.get(commandName).execute(interaction);
	} catch (error) {
		console.error(error);
		await interaction.reply({ content: 'There was an error while executing this command!', ephemeral: true });
	}
});

client.login(process.env.WIKITIKI_BOT_TOKEN);
